/* global before */
require('dotenv').config();
const localConfig = require('./config');
const path = require('path');
const {
  Pact,
  Matchers,
  Verifier,
  broker,
  config
} = require('ob-core-pact-node-wrapper');
const server = require('../server/server');
const { Entitlement } = server.models;
var Promise = require('bluebird');

describe('GET /entitlement/{accessCode}', () => {
  const consumer = 'ob-aisp-direct-debits-channel-service-api';
  const provider = 'ob-cnf-entitlement-enterprise-api';

  before(
    Promise.coroutine(function*() {
      // create relation
      const relation = new Pact(
        Object.assign({}, localConfig.provider, {
          consumer,
          provider
        })
      );

      yield relation.setup().then(() =>
        relation.addInteraction({
          state: 'AISP-DirectDebits:when Entitlement enterprise server is up',
          uponReceiving: 'a request to GET a entitlement record',
          withRequest: {
            method: 'GET',
            path:
              '/entitlement-service/v1.0/entitlements/44151618-dfds-13213-sdfsdf2-213',
            headers: {
              'x-lbg-txn-correlation-id': '1234',
              'x-lbg-internal-user-role': 'SYSTEM',
              'x-lbg-fov-indicator': 'true'
            }
          },
          willRespondWith: {
            status: 200,
            body: {
              EntitlementAccessCode: '44151618-dfds-13213-sdfsdf2-213',
              EntitlementStatus: Matchers.like('string'),
              Scope: Matchers.like('string'),
              Party: {
                UserId: Matchers.like('string'),
                PartyMetadata: {
                  PartyId: Matchers.like('string'),
                  PartyExternalId: Matchers.like('string'),
                  Host: Matchers.like('string')
                }
              },
              Provider: {
                ProviderGrantedPermissions: {
                  Permissions: Matchers.eachLike({
                      Code: Matchers.term({
                        matcher: 'ReadBalances|ReadAccountsBasic|ReadAccountsDetail|ReadBeneficiariesBasic|ReadBeneficiariesDetail|ReadScheduledPaymentsBasic|ReadScheduledPaymentsDetail|ReadStandingOrdersDetail|ReadStandingOrdersBasic|ReadTransactionsBasic|ReadTransactionsDetail|ReadTransactionsCredits|ReadTransactionsDebits|ReadDirectDebits|ReadProducts',
                        generate: 'ReadDirectDebits'
                      })
                    })
                }
              },
              EntitledAgreementResources: Matchers.eachLike({
                AgreementReferenceId: Matchers.like('string'),
                AgreementResourceStatusOnEntitlement: Matchers.like('string'),
                Account: {
                  Name: Matchers.like('string'),
                  SchemeName: Matchers.like('string'),
                  Identification: Matchers.like('string'),
                  SecondaryIdentification: Matchers.like('string')
                },
                Servicer: {
                  SchemeName: Matchers.like('string'),
                  Identification: Matchers.like('string')
                },
                Metadata: {
                  ProductCategory: Matchers.like('string'),
                  BusinessPartyId: Matchers.like('string'),
                  BusinessHost: Matchers.like('string'),
                  ProductType: Matchers.like('string')
                }
              })
            }
          }
        })
      );

      yield Entitlement.getEntitlement(
        '44151618-dfds-13213-sdfsdf2-213',
        '1234',
        'SYSTEM',
        'true'
      );

      yield relation.verify();
      yield relation.finalize();
    })
  );

  it('should publish the contract successfully', () =>
    broker.publishPacts(
      Object.assign({}, localConfig.broker, {
        pactBroker: config.pactBroker,
        providerBaseUrl: process.env.ETN_ENTERPRISE_SERVICE_URL,
        pactFilesOrDirs: [
          path.resolve(
            process.cwd(),
            'contract-test/pacts',
            `${consumer}-${provider}.json`
          )
        ]
      })
    ));
});
