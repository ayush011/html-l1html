# test

> test

## Usage in Development

    ```sh 
    npm start - Node server start
    npm run watch - Node server watcher
    npm run lint - Eslint execution.
    npm test - Unit test execution with Coverage
    npm run test:unit - Only unit test execution
    npm run test:coverage - Only unit test coverage execution
    npm run test:lint - Eslint for unit test
    npm run doc - API tech document generator
    npm run api:def - Swagger json exporter (exports all the modules swagger.json)
    npm run debug - Node server debugger 
    ```
    
###The following steps must be execute in local development environment:

1. Create ".env" file in the root folder
2. Paste the following snippet in the ".env" file and make necessary changes to setup the local development environment.

export DD_ENTERPRISE_SERVICE_URL="http://j2-ob-aisp-dd-enterprise-direct-api-ft-psd2-5310.lbg.eu-gb.mybluemix.net"
export ETN_ENTERPRISE_SERVICE_URL="https://j2-ob-aisp-tactical-mock-services-master.lbg.eu-gb.mybluemix.net"
export ENABLE_RESPONSE_VALIDATION=true


3. run the below command
   npm run local-start

#### License

UNLICENSED © [SAPIENT]
