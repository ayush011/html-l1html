/**
 * Run test.
 *
 * @param String targetBranch
 * @param Map context
 *
 * @return
 */
def runTest(String targetBranch, context) {
    String label = context.config.builder.label
    node(label) {
        checkout scm
        stash name: "pipelines-${context.application}-${targetBranch}", includes: 'pipelines/**'
        this.runTestHandler(targetBranch, context)
    }
}
 
/**
 * Run test handler.
 *
 * @param String targetBranch
 * @param Map context
 *
 * @return
 */
 
def runTestHandler(String targetBranch, context){
    String invokeContractTesting = context.config.bdd.invoke_contract_testing ?: 'fail'
    try {
        sh """source pipelines/scripts/functions && \\
                            ${invokeContractTesting}
                                    """
    } catch (error) {
        echo "FAILED: Contract tests ${error.message}"
        throw error
    } finally {
        step([$class: 'WsCleanup', notFailBuild: true])
    }
}
 
/**
 * Publish Splunk.
 *
 * @param targetBranch
 * @param epoch
 * @param context
 * @param handler
 *
 * @return
 */
def publishSplunk(String targetBranch, String epoch, context, handler) {
    echo "SKIPPING: Not publishing any contract testing related logs."
}
 
/**
 * Name.
 *
 * @return String
 */
String name() {
    return "Contract Testing"
}
 
return this;