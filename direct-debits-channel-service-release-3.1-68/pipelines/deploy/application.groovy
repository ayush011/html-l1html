def deploy(String targetBranch, context){
	String label = context.config.builder.label
	node(label){
		unstash "pipelines"
		this.deployHandler(targetBranch, context)
	}
}
def purge(String targetBranch, context) {
	String label = context.config.builder.label
	if(! targetBranch.startsWith('master')){
		node(label) {
			unstash "pipelines"
			this.purgeHandler(targetBranch, context)
		}
	}
}

def name(){
	return "bluemix"
}

def purgeHandler(String targetBranch, context) {
	bluemixAPI = "${context.config.bluemix.api}"
	bluemixEnv = context.config.bluemix.env ?: ''
	bluemixOrg = context.config.bluemix.org ?: ''

	def app = appName(context, targetBranch)
	withCredentials([
			usernamePassword(credentialsId: 'bluemix-global-deployer',
					passwordVariable: 'BM_PASS',
					usernameVariable: 'BM_USER')
	]) {
		withEnv([
				"BM_API=${bluemixAPI}",
				"BM_ORG=${bluemixOrg}",
				"BM_ENV=${bluemixEnv}",
				"APP=${app}"
		]) {
			try {
			  sh 'pipelines/scripts/cf_destroy.sh'
			} catch (error) {
				echo "Cleanup  failed. Not fatal, Onwards!!"
			} finally {
				step([$class: 'WsCleanup', notFailBuild: true])
			}
		}
	}
}

def deployHandler(String targetBranch, context){
	def app = appName(context, targetBranch)
	def artifact

	dir('j2'){
		deleteDir()
		try {
            unstash "artifactStash"
            artifact = sh(script: "ls *.tar.gz| head -1", returnStdout: true).trim()
            dir('deployable'){
                sh "tar -zxvf  ../${artifact} >/dev/null"

            }
        }catch(error) {
            dir('deployable'){
                checkout scm
            }
        }
	}

	//  manifest
	echo "master-env: ${context.config.environments.master.env}"
	def manifest = buildManifest(app, context.config.environments.master.env , context.config.bluemix)
	writeFile file: "pipelines/conf/manifest.yml", text: manifest
	echo "manifest: ${manifest}"

	// urbancode
    echo "master-tokens: ${context.config.environments.master.tokens}"
    replaceTokens('j2/deployable/urbanCode/', context.config.environments.master.tokens)
    sh("cp -rf j2/deployable/urbanCode/* j2/deployable/  2>/dev/null || : && cp -rf j2/deployable/urbanCode/.* j2/deployable/ 2>/dev/null || :")
	stash name: "libertyArtifactStash", includes: '**'
	// deploy
	def serviceEnvs = [:]
	serviceEnvs["deployable"] = "j2/deployable"
	serviceEnvs["APP"] = app
	try{
		String label_liberty = context.config.builder.label_liberty
		node(label_liberty) {
			dir("target") {
			deleteDir()
			unstash "libertyArtifactStash"
			sh "ls -ltr"
			sh "echo sh pipelines/scripts/liberty_node_deploy.sh ${app} ${targetBranch}"
			sh "echo Sleeping for debug"
			sh "sleep 30"
			sh "sh pipelines/scripts/liberty_node_deploy.sh ${app} ${targetBranch}"
			([$class: 'WsCleanup', notFailBuild: true])
			//sh 'pipelines/scripts/cf_deploy.sh'
			}
		}	
	} catch(error) {
			echo "FAILED: Liberty Deployment"
				throw error
		} finally{
			archive 'pipelines/conf/manifest.yml'
			step([$class: 'WsCleanup', notFailBuild: true])
		}
}

return this;
