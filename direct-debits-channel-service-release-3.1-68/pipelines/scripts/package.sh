#!/bin/bash


WORKSPACE=${WORKSPACE:-`pwd`}
DOCKERWORKSPACE=/home/jenkins2/workspace/
artifact=$1
version=$2
COMMAND="touch -c * ;  echo -e \"\nartifact_version=$version\" >> /home/jenkins2/workspace/microservice.metadata;
                                ln -sf ../node_modules ; \
                                npm prune --production && \
                                rm -rf j2 && mkdir -p j2  && \
                                tar --warning=no-file-removed \
				--warning=no-file-changed \
                                --dereference \
                                --ignore-failed-read \
                                --exclude='*.tar.gz' \
                                --exclude='.cfignore' \
                                --exclude='.git*' \
                                --exclude='.eslint*' \
                                --exclude='pipelines' \
                                --exclude='Jenkinsfile' \
                                --exclude='specs' \
                                --exclude='Dockerfile.*' \
                                --exclude='docker-*.*' \
                                --exclude='.dockerignore' \
                                --exclude='.apiconnect' \
                                --exclude='*.log' \
                                --exclude='log' \
                                --exclude='logs' \
                                --exclude='component-test' \
                                --exclude='coverage' \
                                --exclude='j2' \
                                -zcf j2/${artifact} \
                                . || true "


source ${WORKSPACE}/pipelines/scripts/functions
set -ex
hostname && pwd

touch -c -t 12101630  * || true
rm -rf node_modules
docker-compose build node-allmodules
docker-compose run --rm node-allmodules bash -c "${COMMAND}"
ls -laR j2 || true
docker-compose kill || true
docker-compose rm -f || true
