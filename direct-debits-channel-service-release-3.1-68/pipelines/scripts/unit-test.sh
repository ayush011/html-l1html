#!/bin/bash

WORKSPACE=${WORKSPACE:-`pwd`}
DOCKERWORKSPACE=/home/jenkins2/workspace

source ${WORKSPACE}/pipelines/scripts/functions
set -ex
hostname && pwd

touch -c -t 12101630  * || true
rm -rf node_modules
docker-compose build node-allmodules
export COMPOSE_HTTP_TIMEOUT=200
docker-compose run --rm node-allmodules bash -c "ln -sf ../node_modules; npm test"
docker-compose kill || true
docker-compose rm -f || true

find coverage/ -type f | xargs sed -i 's+'${DOCKERWORKSPACE}'/++g' || true
