#!/bin/bash

echo targetEnv is :-  ${targetEnvironment}
echo application is :- ${appname}
set -ex

function deployBluemix {
cf logout
cf login -a $BM_API -u $BM_USER -p $BM_PASS -o ${BM_ORG} -s ${BM_ENV}
cf delete ${APP} -f -r || echo "Failed to delete application."
cf push -f manifest.yml -k 1024M
}

cd $deployable
cp ${WORKSPACE}/pipelines/conf/manifest.yml  manifest.yml
cat manifest.yml
ls -la
deployBluemix

