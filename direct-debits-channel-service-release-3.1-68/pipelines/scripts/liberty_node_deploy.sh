#!/bin/bash
####################################################################################################
#Script Name	: liberty_nodejs_deploy.sh <App Zip Name>
#Example        : ./liberty_nodejs_deploy.sh ob-cnf-auth-enterprise-api.zip
#Description	: NodeJS micro service deployment on liberty serever
#Args           : App Zip Name is arguments
####################################################################################################
set +e
workingDir=`pwd`
#Args pass check
if [ $# -ne 2 ]; then
    echo "$0: usage: $0 <App Zip Name>"
    echo "example usage: $0 ob-cnf-auth-enterprise-api.zip"
    exit 1
fi
artefactName=$1
targetBranch=$2
echo "Parameters :: $artefactName $targetBranch"
WORKSPACE=${WORKSPACE}
echo "WORKSPACE is ${WORKSPACE}"
sh /opt/apps/deployscript/liberty_node_deploy.sh $artefactName $targetBranch $WORKSPACE
