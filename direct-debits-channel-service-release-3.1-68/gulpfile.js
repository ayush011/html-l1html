'use strict';

require('dotenv').config();
var gulp = require('gulp');
var argv = require('yargs').argv;
const apiConfigFilePath = require('ob-core-bdd').apiConfigFilePath;
const putMockData = require('ob-core-bdd').putMockData;
const runSequence = require('run-sequence');

const endpointConfig = require('./component-test/host-system-mocks/config.js');

process.env.ff = (argv.ff) ? argv.ff : process.env.ff;
process.env.tags = (argv.tags) ? argv.tags : process.env.tags;

var plugins = {
    gulpWebDriver: require('gulp-webdriver'),
    path: require('path'),
    glob: require('glob'),
    fs: require('fs'),
    exec: require('child_process').exec,
    wdio: require('core-cucumber-bdd')
};

gulp.task('wdio-aisp', function () {

    var isWin = /^win/.test(process.platform);
    var cmd = isWin ? 'wdio.cmd' : 'wdio';

    return gulp.src(apiConfigFilePath)
        .pipe(plugins.wdio({
            wdioBin: plugins.path.join(__dirname, 'node_modules', '.bin', cmd)
        }));
});

gulp.task('wdio-aisp-wm', function () {

    putMockData(endpointConfig).then(function () {

        var isWin = /^win/.test(process.platform);
        var cmd = isWin ? 'wdio.cmd' : 'wdio';

        return gulp.src(apiConfigFilePath)
            .pipe(plugins.wdio({
                wdioBin: plugins.path.join(__dirname, 'node_modules', '.bin', cmd)
            }));
    }).catch(e => {
        console.log('Error running bdd tests' + e);
        throw new Error(e);
    });
});

gulp.task('put-mock', function () {
    putMockData(endpointConfig).then(function () {
        console.log('Uploading mock completed');
    }).catch(e => {
        throw new Error(e);
    });
});

var nodeServer;

gulp.task('run-server', function () {

    nodeServer = plugins.exec('node .');
});

gulp.task('kill-server', function () {
    var isWin = /^win/.test(process.platform);

    isWin ? process.exit() : nodeServer.kill('SIGHUP');
});

gulp.task('run-local-bdd', (cb) => {
    process.env.LOCAL_BDD = true;

    return runSequence('run-server', 'wdio-aisp', 'kill-server', cb);
});

gulp.task('run-local-bdd-wm', (cb) => {
    process.env.LOCAL_BDD = true;

    return runSequence('run-server', 'put-mock', 'wdio-aisp', 'kill-server', cb);
});

gulp.task('run-api', ['wdio-aisp'], (cb) => cb());

gulp.task('run-api-wm', ['put-mock', 'wdio-aisp'], (cb) => cb());
