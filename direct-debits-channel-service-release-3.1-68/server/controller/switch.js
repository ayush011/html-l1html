'use strict';
const logger = require('ob-core-node-logger').app;
const {CIRCUIT_BREAKER} = require('../enums/direct-debits-constants');
const switch_constants = require('../enums/switch-constants');
const extractSwitchValue = require('../filters/extractSwitchValue');
const {SWITCH_NAME,BRAND} = require("../enums/switch-constants");
const {readCache,updateCache} = require("../utils/cacheController");
const {cache_time_limit} = require('../enums/switch-constants');


function fetchSwitches() {
    let [DirectDebits,correlationID] = arguments;
    let localCache = readCache();
    let diff = diffEvaluator(localCache);

    if(diff>=cache_time_limit){
        let switches = DirectDebits.app.models.Switch;
        let params =[correlationID,BRAND];
        logger.debug('Before switch api call');
        return switches.circuitBreaker.execute(switches,switch_constants.CONNECTOR_METHOD_NAME, params, CIRCUIT_BREAKER.DD_REMOTE_METHOD)
        .then(switches => updateCache(switches))
        .then(switches => extractSwitchValue(switches,SWITCH_NAME))
        .catch(err => {
            /* istanbul ignore next */
            throw err;
        });


    }else{
        return new Promise((resolve) => {
            logger.debug('Reading switch value from local cache');
            let value = extractSwitchValue(localCache['switches'],SWITCH_NAME);
            resolve(value);
        });
    }
}

function diffEvaluator(localCache){
    logger.debug('Evaluating difference since last local cache was updated');
    let lastUpdated = new Date(localCache['lastUpdated']);
    if(lastUpdated){
        let currentDateTime = new Date();
        let diff = (currentDateTime - lastUpdated);
        return diff;
    }else{
        return cache_time_limit;
    }
}

module.exports = {
    fetchSwitches
};
