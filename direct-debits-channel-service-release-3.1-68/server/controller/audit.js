const logger = require('ob-core-node-logger').app;
const {CIRCUIT_BREAKER} = require('../enums/direct-debits-constants');

module.exports = function() {

  logger.debug('Call recieved to create Audit : controller/audit');
  const [Audit, auditRecord, ctx] = arguments;
  const reqHeaders = ctx.active.http.req.headers;
  const headers = auditRecord.requestHeaders || reqHeaders || {};
  const auditRequest = auditRecord.requestBody || {};

  const InternalUserRole = headers['x-lbg-internal-user-id'] || 'undefined';
  const CorrelationID = headers['x-lbg-txn-correlation-id'] || 'undefined';
  const SessionId = headers['x-lbg-session-id'] || reqHeaders['x-lbg-session-id'];
  const Channel = headers['x-lbg-channel'];
  const Brand = headers['x-lbg-brand'];
  const InternalPartyId = headers['x-lbg-internal-party-id'];
  const FovIndicator = headers['x-lbg-fov-indicator'];
  const PartyHost = headers['x-lbg-party-host'] || reqHeaders['x-lbg-party-host'];
  const PartyId = headers['x-lbg-party-id'] || reqHeaders['x-lbg-party-id'];
  const SourceChannelName = headers['x-lbg-source-channel-name'];
  const ExternalUserId = headers['x-lbg-external-user-id'];

  return Audit.circuitBreaker.execute(Audit, CIRCUIT_BREAKER.AUDIT_SERVICE_METHOD, [InternalUserRole, CorrelationID,
    SessionId, Channel, Brand, InternalPartyId, FovIndicator, PartyHost, PartyId, SourceChannelName, ExternalUserId,
    auditRequest], CIRCUIT_BREAKER.AUDIT_SERVICE_METHOD);
};
