'use strict';

module.exports = function (accountId, directDebits) {
  const filteredDirectDebits = [];

  if (Array.isArray(directDebits)) {
    directDebits.forEach(function (currentValue) {
      if (currentValue.AgreementReferenceId === accountId) {
        filteredDirectDebits.push(currentValue);
      }
    });
  }
  return filteredDirectDebits;
};
