'use strict';

module.exports = function (Switches, SwitchName) {
  const required_switch = Switches.filter(item => item.SwitchName === SwitchName);
  if(required_switch.length !== 0){
      return required_switch[0].SwitchOn;
  }else{
      return false;
  }
};
