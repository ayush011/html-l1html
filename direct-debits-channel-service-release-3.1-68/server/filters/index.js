'use strict';

const directDebitsFilter = require('./direct-debits');

module.exports = {
  directDebitsFilter
};
