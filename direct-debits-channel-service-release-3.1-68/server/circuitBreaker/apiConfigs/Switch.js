module.exports = {
    serviceName: 'getSwitchValue',
    timeout: process.env.SWITCH_CB_TIMEOUT || 30000,
    openTimeoutInMilliseconds: process.env.SWITCH_CB_OPEN_TIMEOUT || 50000,
    errorThresholdPercentage: process.env.SWITCH_CB_ERROR_THRESHOLD_PERCENTAGE || 50,
    minimumRequestForHealthCheck: process.env.SWITCH_CB_MIN_REQ_FOR_HEALTH_CHECK || 10,
    executionTrackWindow: process.env.SWITCH_CB_EXECUTION_TRACK_WINDOW || 10000,
    windowBucket: process.env.SWITCH_CB_WINDOW_BUCKET || 10,
    disable: process.env.SWITCH_CB_DISABLE || false,
    downstream: process.env.SWITCH_CB_DOWNSTREAM || 'enterprise'
  };