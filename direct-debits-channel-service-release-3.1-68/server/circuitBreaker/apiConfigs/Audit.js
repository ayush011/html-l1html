module.exports = {
  serviceName: 'submitAuditEvent',
  timeout: process.env.AUDIT_V2_CB_TIMEOUT || 30000,
  openTimeoutInMilliseconds: process.env.AUDIT_V2_CB_OPEN_TIMEOUT || 50000,
  errorThresholdPercentage: process.env.AUDIT_V2_CB_ERROR_THRESHOLD_PERCENTAGE || 50,
  minimumRequestForHealthCheck: process.env.AUDIT_V2_CB_MIN_REQ_HEALTH_CHECK || 10,//min number calls failing
  executionTrackWindow: process.env.AUDIT_V2_CB_EXECUTION_TRACK_WINDOW || 10000,//time span
  windowBucket: process.env.AUDIT_V2_CB_WINDOW_BUCKET || 10,
  disable: process.env.AUDIT_V2_CB_DISABLE || false,
  downstream: process.env.AUDIT_V2_CB_DOWNSTREAM || 'audit_v2'
};
