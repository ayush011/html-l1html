module.exports = {
  serviceName: 'getDirectDebits',
  timeout: process.env.DIRECT_DEBITS_CB_TIMEOUT || 30000,
  openTimeoutInMilliseconds: process.env.DIRECT_DEBITS_CB_OPEN_TIMEOUT || 50000,
  errorThresholdPercentage: process.env.DIRECT_DEBITS_CB_ERROR_THRESHOLD_PERCENTAGE || 50,
  minimumRequestForHealthCheck: process.env.DIRECT_DEBITS_CB_MIN_REQ_HEALTH_CHECK || 10, //min number calls failing
  executionTrackWindow: process.env.DIRECT_DEBITS_CB_EXECUTION_TRACK_WINDOW || 10000, //time span
  windowBucket: process.env.DIRECT_DEBITS_CB_WINDOW_BUCKET || 10,
  disable: process.env.DIRECT_DEBITS_CB_DISABLE || false,
  downstream: process.env.DIRECT_DEBITS_CB_DOWNSTREAM || 'enterprise'
};

/**for the time span (10s) if the min numbr of calls are failing(10),
 * with either response timeout error or hitting the error threshold percentage (50)
 * with other network error, then the circuit will open and will not allow
 * calls to the downstream for the duration of the openTimeout (50s).
 */
