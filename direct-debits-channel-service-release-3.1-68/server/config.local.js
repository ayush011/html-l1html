'use strict';
const {errorHandlerV3} = require('ob-aisp-common-module');
const config = require('./config.json');
const restApiRoot = require('./utils/getRootContext');

const params = {
  metaFlag: false,
  filename: __filename
};

module.exports = {
    restApiRoot: restApiRoot(),
    host: process.env.VCAP_APP_HOST || process.env.APP_HOST || config.host,
    port: process.env.VCAP_APP_PORT || process.env.APP_PORT || config.port,
    remoting: {
        errorHandler: {
            handler: errorHandlerV3(params)
        }
    }
};


