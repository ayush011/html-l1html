const restApiRoot = require('./utils/getRootContext');

module.exports = {
  'loopback-component-explorer': {
    'mountPath': `${restApiRoot()}/explorer`
  }
};
