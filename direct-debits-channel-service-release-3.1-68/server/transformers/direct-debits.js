'use strict';

const templates = require('../templates');
const StatusCodeMapping = require('../enums/direct-debits-constants').DIRECT_DEBIT_STATUS;
const logger = require('ob-core-node-logger').app;


module.exports = function (hostName, accountId, permissions, req, remoteOutput) {
  const grantedPermissions = permissions.permissions;
  const template = selectTemplate(grantedPermissions);
  const tranformInputData = remoteOutput[0] && remoteOutput[0].DirectDebits;
  const tranformOutputData = transform(tranformInputData, accountId, template, hostName);
  return tranformOutputData;
};

function transform(directDebits, accountId, template, hostName) {
  if(!directDebits.length){
    logger.debug('No direct debits record found');
  }
  let transformedOutput = {
    Data: {
      DirectDebit: []
    },
    Links: templates.Links(hostName, accountId),
    Meta: templates.Meta()
  };

  transformedOutput.Data.DirectDebit = directDebits.filter(directDebit =>{
       return  StatusCodeMapping[directDebit.StatusCode];
  })
    .map(dd => template(dd, accountId));
  return transformedOutput;
}

function selectTemplate(permissions) {
  const outputTemplate = [];
  permissions.forEach(function (permission) {
    if (templates[permission]) {
      outputTemplate.push(templates[permission]);
    }
  });
  return outputTemplate[0];
}
