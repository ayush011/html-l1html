'use strict';

const entitlementConstants = require('../../enums/entitlement');
const entitlementDataValidation = require('ob-aisp-common-module').entitlementDataValidation;
const exceptionConstants = require('ob-aisp-common-module').exceptionConstants;
const {securityErrorHandler} = require('../../utils/security-logger');
const logger = require('ob-core-node-logger').app;

module.exports = function (accountId, entitlement) {
  try {
    entitlementDataValidation(accountId, entitlement, entitlementConstants);
    logger.debug('Entitlement validation passed');
    return entitlement;
  } catch (e) {
    logger.error('Entitlement Data Validation failed- ', e.message);
    securityErrorHandler(exceptionConstants.UNAUTHORISED);
    throw e;
  }
};
