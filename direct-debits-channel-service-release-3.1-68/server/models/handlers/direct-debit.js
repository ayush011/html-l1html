'use strict';

const accountFilter = require('../../filters').directDebitsFilter;
const CB_CONSTANTS = require('../../enums/direct-debits-constants').CIRCUIT_BREAKER;
const COMMERCIAL = require('../../enums/channel-constants').COMMERCIAL;

const {logAccountDetails} = require('../../../server/utils/log-account-utils');
const logger = require('ob-core-node-logger').app;
const EMPTY_DIRECT_DEBITS = [{DirectDebits: []}];
const {fetchSwitches} = require('../../controller/switch');

module.exports = function ({DirectDebits, accountId, correlationID, brand, fovIndicator, entitlement, channel}) {
  if (channel === COMMERCIAL) {
    // if channel is commercial, return an empty data set
    return Promise.resolve(EMPTY_DIRECT_DEBITS);
  }

  const accounts = entitlement.EntitledAgreementResources;
  const filteredAccount = accountFilter(accountId, accounts);

  const accountDirectDebits = filteredAccount.map((account) => {
    const params = [account.Account.Identification, account.Servicer.Identification, correlationID, brand, fovIndicator];
    logAccountDetails(logger, account.Account.Identification, account.Servicer.Identification);
    logger.debug('getDirectDebits using params: ', params);

    /* IBC SWITCH IMPLEMENTATION */
    return fetchSwitches(DirectDebits,correlationID)
    .then(switch_value =>{
      if(Boolean(switch_value)) {
        const EaadsDirectDebits = DirectDebits.app.models.EaadsDirectDebits;
        return EaadsDirectDebits.circuitBreaker.execute(EaadsDirectDebits, CB_CONSTANTS.DD_DS_OPERATION, params,
          CB_CONSTANTS.DD_REMOTE_METHOD);
      } else {
        return DirectDebits.circuitBreaker.execute(DirectDebits, CB_CONSTANTS.DD_DS_OPERATION, params,
          CB_CONSTANTS.DD_REMOTE_METHOD);
      }
    });

  });

  return Promise.all(accountDirectDebits);
};
