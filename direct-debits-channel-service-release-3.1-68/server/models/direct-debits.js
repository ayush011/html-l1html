'use strict';
const loopbackContext = require('loopback-context');
const checkPermission = require('ob-aisp-common-module').checkPermission;
const {checkConsentExpiry} = require('ob-aisp-common-module');
const exceptionConstants = require('ob-aisp-common-module').exceptionConstants;
const permissions = require('../enums/direct-debits-permission').permissions;
const transform = require('../transformers/direct-debits');
const getDirectDebitsData = require('./handlers/direct-debit');
const checkStatus = require('./handlers/status-check');
const CB_CONSTANTS = require('../enums/direct-debits-constants').CIRCUIT_BREAKER;
const {securityErrorHandler} = require('../utils/security-logger');
const {setEntitlementToCtx, logAuditEvent} = require('../utils/auditHelpers');
const {checkProductCategoryValidity} = require ('../utils/validate-product-category');



module.exports = function (DirectDebits) {

  DirectDebits.getDirectDebits = function () {
    let [authorization, accountId, correlationID, brand, channel, userRole, fapiID, hostName, fovIndicator, req, cb] = arguments;

    if (!authorization) {
      securityErrorHandler(exceptionConstants.UNAUTHORISED);
      throw exceptionConstants.UNAUTHORISED;
    }

    let grantedPermissions = {};

    const Entitlement = DirectDebits.app.models.Entitlement;
    const Audit = DirectDebits.app.models.Audit;
    const params = [authorization, correlationID, userRole, fovIndicator, brand, channel, fapiID];

    // Loopback context to get, set data required for audit event log
    let ctx = loopbackContext.getCurrentContext();
    let error;

    Entitlement.circuitBreaker.execute(Entitlement, CB_CONSTANTS.ENTITLEMENT_DS_OPERATION, params, CB_CONSTANTS.ENTITLEMENT_REMOTE_METHOD)
      .then(ent => setEntitlementToCtx(ctx, ent, accountId))
      .then(ent => checkStatus(accountId, ent))
      .then(ent => checkPermission(grantedPermissions, permissions, ent))
      .then(ent => {
        const unrestrictedAccessDuration= parseInt(process.env.UNRESTRICTED_ACCESS_DURATION);
        if(unrestrictedAccessDuration){
          checkConsentExpiry(ent, unrestrictedAccessDuration);
        }
        return ent;
      })
      .then(ent => {
                    let result = [
                      {DirectDebits: []}
                    ];
                    if (checkProductCategoryValidity(ent, accountId, ctx) ) {
                      result = getDirectDebitsData({DirectDebits, accountId, correlationID, brand, fovIndicator, entitlement: ent, channel});
                  }
                  return result;
                }
      )
      .then(ent => transform(hostName, accountId, grantedPermissions, req, ent))
      .then(transformedData => cb(null, transformedData))
      .catch(err => {
        error = err;
        if (!err.isStatusError) {
          securityErrorHandler(err);
        }
        cb(err);
      })
      .finally(()=> {
        logAuditEvent(ctx, error, Audit);
      });
  };
};
