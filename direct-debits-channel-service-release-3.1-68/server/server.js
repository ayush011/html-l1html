'use strict';
require('dotenv').config();
const analytics = require('ob-tpp-lbg-analytics');

const logger = require('ob-core-node-logger').app;

require('dnscache')({
  enable: true,
  ttl: 300,
  cachesize: 1000,
});
const ENABLE_ANALYTICS = process.env.ENABLE_ANALYTICS || 'FALSE';

const loadAppdynamics = (enableAnalytics, logger) => {
  if (enableAnalytics.toUpperCase() === 'TRUE') {
    try {
      analytics.inspect({logger});
      logger.info('Analytics initialized.');
    } catch (err) {
      logger.error('Analytics failed to initialize.', err.message);
    }
  }
};


loadAppdynamics(ENABLE_ANALYTICS, logger);
const boot = require('loopback-boot');
const api = require('./api');
const {logWarning} = require('./utils/server-utils');

//log warning stack
logWarning(logger);



// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(api, __dirname, (err) => {
  if (err) {
    throw err;
  }

  // start the server if `$ node server.js`
  if (require.main === module) {
    api.start();
  }
});

module.exports = api;
