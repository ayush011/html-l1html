const ENABLE_LOGGING_ACCOUNT_DETAILS = process.env.ENABLE_LOGGING_ACCOUNT_DETAILS || 'FALSE';

const logAccountDetails = (logger, accountNumber, sortCode) => {
    if (ENABLE_LOGGING_ACCOUNT_DETAILS.toUpperCase() === 'TRUE') {
        logger.store.setAccountNumber(accountNumber);
        logger.store.setSortCode(sortCode);
    }
  };

  module.exports = {
    logAccountDetails
  };
