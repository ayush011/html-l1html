'use strict';

const moment = require('moment-timezone');
const DIRECT_DEBITS_ENUMS = require('./../enums/direct-debits-constants');

function dateFormater(date) {
  if(!date) {
return date;
}  else if(date.length <= 10){
    // when no time is present a default time is added: T00:00:00Z
    date= `${date}T00:00:00Z`;
    return moment(date).tz('Etc/GMT').format(DIRECT_DEBITS_ENUMS.DATE_TIME_FORMAT);
  }  else{
    // when time is present, the same time is returned after converting to GMT time zone
    return moment(date).tz('Etc/GMT').format(DIRECT_DEBITS_ENUMS.DATE_TIME_FORMAT);
  }
}

module.exports = {
    dateFormater
};
