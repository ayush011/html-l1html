'use strict';

const emptyGroup= require('../enums/direct-debits-constants').EMPTY_RESPONSE_GROUP;
const accountFilter = require('../filters').directDebitsFilter;
const {detemineProductCategory} = require('../utils/determine-product-category');


module.exports.checkProductCategoryValidity = function ( ent, accountId, ctx ) {
    const accounts = ent.EntitledAgreementResources;
    const filteredAccount = accountFilter(accountId, accounts, ctx);
    return (filteredAccount.length > 0) ? detemineProductCategory(emptyGroup, filteredAccount[0]) : true ;
};
