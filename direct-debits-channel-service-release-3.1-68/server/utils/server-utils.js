const fs = require('fs');
const path = require('path');
const {outboundTLS} = require('ob-core-common-modules');

const loadDataSource = (ddsLocation, app) => {
    if (ddsLocation) {
        fs.readdirSync(ddsLocation).filter((file) => {
            const fPath = path.join(ddsLocation, file);
            if (fs.statSync(fPath).isDirectory()) {
                let json;
                let configFile;
                /*eslint-disable*/
                configFile = 'datasource';
                json = require(path.resolve(fPath, configFile));
                /*eslint-enable*/
                let updatedJSON = outboundTLS(json);
                app.dataSource(file, updatedJSON);
            }
            return true;
        });
    }
};

const logWarning = (logger) => {
    process.on('warning', (warning) => {
      logger.error(`WARNING: ${warning.name}`,{message:warning.message,stack:warning.stack});
    });
  };

module.exports = {
    loadDataSource,
    logWarning
};

