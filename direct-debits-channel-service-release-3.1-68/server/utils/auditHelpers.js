const {auditErrorMap, auditTemplate, auditEvents} = require('ob-aisp-common-module');
const {auditLogger, initialize} = require('ob-core-audit-logger');
const logger = require('ob-core-node-logger').app;
const AUDIT_CONST = require('../enums/audit-constants');
const auditV2 = require('../controller/audit');
const COMMERCIAL = require('../enums/channel-constants').COMMERCIAL;


const setEntitlementToCtx = function (ctx, ent, accountId) {
  if (ctx) {
    ctx.set(AUDIT_CONST.entitlement, ent);
    ctx.set(AUDIT_CONST.accountId, accountId);
  }

  return ent;
};

const isChannelCC = (ctx) => {
  const headers = ctx ? ctx.active.http.req.headers : null;
  const channel = headers && headers['x-lbg-channel'];
  return channel && channel.toUpperCase() === COMMERCIAL;
};

const getAuditData = function (ctx, error) {
  let auditEvent = error ? auditErrorMap(error.title, AUDIT_CONST.service) : auditEvents.successEvent(AUDIT_CONST.service);
  let auditData;

  if (auditEvent) {
    if (isChannelCC(ctx)) {
      auditEvent = {
        EventType: AUDIT_CONST.CommercialEventType,
        ErrorCode: auditEvent.ErrorNumber,
        EventLogText: error && error.message
      };
    }
    ctx = auditTemplate(ctx, auditEvent, AUDIT_CONST.template);
    auditData = ctx.get(AUDIT_CONST.key);
  }

  return auditData;
};

const logAuditEvent = function (ctx, error, Audit) {
  initialize(logger);
  const auditData = getAuditData(ctx, error);
  logger.debug('Audit logger remote api call ', auditData);

  if (auditData) {
    (isChannelCC(ctx) ? auditV2(Audit, auditData, ctx) : auditLogger(auditData))
      .then(() => {
        logger.info('AUDIT LOG: Audit event successfully logged');
      })
      .catch(() => {
        logger.error('AUDIT LOG: Audit event api failed');
      });
  } else {
    logger.info('AUDIT LOG: No Audit data found for error');
  }
};


module.exports = {
  setEntitlementToCtx,
  logAuditEvent
};
