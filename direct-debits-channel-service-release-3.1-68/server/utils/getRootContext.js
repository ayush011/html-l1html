'use strict';

const fs = require('fs');
const config = require('../config.json');

const getRootContext = (serverFile = 'server.json') => {
  let CONTEXT_ROOT;
  if (fs.existsSync(serverFile)) {
    const joinConfig = JSON.parse(fs.readFileSync(serverFile, 'utf8'));
    CONTEXT_ROOT = `/${joinConfig.contextRoot}`;
  } else {
    CONTEXT_ROOT = config.restApiRoot;
  }
  return CONTEXT_ROOT;
};

module.exports = getRootContext;
