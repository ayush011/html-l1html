const {SECURITY} = require('../enums/direct-debits-constants');
const {securityLoggerInit} = require('ob-core-security-logger');
const securityLogger = securityLoggerInit(__filename);

function securityErrorHandler(err){
    let errorCode = err.status || err.statusCode;
    switch(errorCode){
        case 401:
        {
            securityLogger.info(SECURITY.INVALID_ENTITLEMENT_SCOPE.EVENT_CODE, SECURITY.INVALID_ENTITLEMENT_SCOPE.EVENT_MESSAGE);
            break;
        }
        case 403:
        {
            securityLogger.info(SECURITY.INVALID_ENTITLEMENT_PERMISSION.EVENT_CODE, SECURITY.INVALID_ENTITLEMENT_PERMISSION.EVENT_MESSAGE);
            break;
        }
        case 404:
        {
            securityLogger.info(SECURITY.INVALID_ENTITLEMENT_ID.EVENT_CODE, SECURITY.INVALID_ENTITLEMENT_ID.EVENT_MESSAGE);
            break;
        }
        default:
            break;
    }
}
module.exports = {
    securityErrorHandler
};
