'use strict';

module.exports.detemineProductCategory = function(emptyGroup, filtered){
    return emptyGroup.indexOf(filtered.Metadata.ProductCategory) === -1;
};
