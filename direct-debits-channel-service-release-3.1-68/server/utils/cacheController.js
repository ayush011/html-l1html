var fs = require('fs');
const path = require('path');
const logger = require('ob-core-node-logger').app;

const updateCache = (data) => {
    const switchesPath = path.resolve(__dirname,'../enums/switches.json');
    let fileData = {};
    fileData['switches'] = data;
    fileData['lastUpdated'] = new Date();
    logger.debug('Updating local cache with switch api response');
    fs.writeFileSync(switchesPath, JSON.stringify(fileData));
    return data;
};

const readCache = () => {
    const switchesPath = path.resolve(__dirname,'../enums/switches.json');
    let data = fs.readFileSync(switchesPath, {encoding:'utf8', flag:'r'});
    return JSON.parse(data);
};

module.exports = {
    readCache,
    updateCache
};
