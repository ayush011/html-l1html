const ReadDirectDebits = require('./read-direct-debits');
const Links = require('./links');
const Meta = require('./meta');

module.exports = {
  ReadDirectDebits,
  Links,
  Meta
};
