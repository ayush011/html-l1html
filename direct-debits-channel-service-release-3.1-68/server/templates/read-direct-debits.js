'use strict';
const prune = require('ob-aisp-common-module').pruneObject;
const DDConstants = require('../enums/direct-debits-constants');
const helper = require('../utils/helper');

module.exports = function (directDebits, accountId) {
  const MandateIdentification = directDebits.Originator.AUDDISRefValue || directDebits.Originator.RefText || DDConstants.NOT_AVAILABLE;
  let readDirectDebits;
  const PreviousPaymentAmount = directDebits.PreviousPayment && directDebits.PreviousPayment.Amount ? previousPaymentAmount(directDebits) : '';
  const DirectDebitRecord = {
      AccountId: accountId,
      MandateIdentification: MandateIdentification,
      DirectDebitStatusCode: DDConstants.DIRECT_DEBIT_STATUS[directDebits.StatusCode],
      Name: directDebits.Originator && directDebits.Originator.Name || DDConstants.NOT_AVAILABLE,
      PreviousPaymentDateTime: helper.dateFormater(directDebits.PreviousPayment && directDebits.PreviousPayment.DateTime),
      PreviousPaymentAmount: PreviousPaymentAmount,
      Frequency: DDConstants.DIRECT_DEBIT_FREQUENCIES[directDebits.Originator.Frequency]
  };
  readDirectDebits = prune(DirectDebitRecord);

  return readDirectDebits;
};

function previousPaymentAmount(directDebits) {
  return {
    Amount: directDebits.PreviousPayment && directDebits.PreviousPayment.Amount,
    Currency: directDebits.PreviousPayment && directDebits.PreviousPayment.CurrencyCode
  };
}
