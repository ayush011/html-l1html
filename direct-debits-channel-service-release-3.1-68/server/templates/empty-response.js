const {SELF_LINK, SELF_LINK_REGEX} = require('../enums/direct-debits-constants');

function createEmptyResponse(hostName, accountId) {
    let params = {hostName, accountId};
    return {
        'Data' : {},
        'Links' : {
        'Self' : SELF_LINK.replace(SELF_LINK_REGEX, (m)=> params[m])
        },
        'Meta': {}
    };
}

module.exports={
  createEmptyResponse
};
