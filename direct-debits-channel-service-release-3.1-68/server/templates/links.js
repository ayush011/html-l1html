'use strict';

const {SELF_LINK, SELF_LINK_REGEX} = require('../enums/direct-debits-constants');

module.exports = (hostName, accountId) => {
let params = {hostName, accountId};
  return {
      Self: SELF_LINK.replace(SELF_LINK_REGEX, (m)=> params[m])
  };
};
