const {errorHandlerV3} = require('ob-aisp-common-module');
const loggerInitialize = (app, logger) => {
  const params = {
    type: 'error',
    metaFlag: false,
    filename: __filename,
    showBody: false,
    correlationKey: 'x-lbg-txn-correlation-id',
    category: 'APP'
  };
  app.middleware('final:after', '', errorHandlerV3(params, logger));
};
module.exports = {
  loggerInitialize
};

