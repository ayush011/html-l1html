module.exports = {
  template: 'common',
  key: 'audit_log_data',
  entitlement: 'entitlement',
  agreementResource: 'agreementResource',
  service: 'directDebit',
  accountId: 'accountId',
  CommercialEventType: 'OB022'
};
