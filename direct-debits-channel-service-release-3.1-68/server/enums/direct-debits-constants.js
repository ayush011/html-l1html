module.exports = {
  DIRECT_DEBIT_STATUS: {
    '1': 'Active',
    '4': 'Inactive',
    '8': 'Active',
    '9': 'Inactive',
    '14': 'Active'
  },
  NOT_AVAILABLE: 'NOT AVAILABLE',
  constants: {
    DIRECT_DEBIT_TRANSACTION: 'requestDirectDebitsTransactions',
    DIRECT_DEBIT_TRANSFORMATION: 'Direct Debits Transformation'
  },
  CIRCUIT_BREAKER: {
    DD_REMOTE_METHOD: 'getDirectDebits',
    DD_DS_OPERATION: 'getDirectDebitsDetails',
    ENTITLEMENT_REMOTE_METHOD: 'getEntitlement',
    ENTITLEMENT_DS_OPERATION: 'getEntitlement',
    AUDIT_SERVICE_METHOD: 'submitAuditEvent'
  },
  SECURITY: {
    INVALID_ENTITLEMENT_ID: {
      EVENT_CODE: 'EAS_API_ERR_004',
      EVENT_MESSAGE: 'Entitlement not found.'
    },
    INVALID_ENTITLEMENT_SCOPE: {
      EVENT_CODE: 'EGS_API_ERR_004',
      EVENT_MESSAGE: 'User does not have correct status or scope to access the requested resource.'
    },
    INVALID_ENTITLEMENT_PERMISSION: {
      EVENT_CODE: 'EGS_API_ERR_002',
      EVENT_MESSAGE: 'User doesn\'t have correct permissions.'
    }
  },
  APP_NAME: 'Balance channel service',
  AUDIT_EVENT_TEXT: 'Data pushed to API Gateway for Balance',
  SELF_LINK: 'hostName/accounts/accountId/direct-debits',
  SELF_LINK_REGEX: /hostName|accountId/gi,
  DATE_TIME_FORMAT: 'YYYY-MM-DDTHH:mm:ssZ',
  EMPTY_RESPONSE_GROUP: ['PCC', 'BCC'],
  DIRECT_DEBIT_FREQUENCIES: {
    '0': 'UK.OBIE.NotKnown',
    '1': 'UK.OBIE.Monthly',
    '2': 'UK.OBIE.Quarterly',
    '3': 'UK.OBIE.HalfYearly',
    '4': 'UK.OBIE.Annual'
  },
};
