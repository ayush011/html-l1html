'use strict';

const loopback = require('loopback');

const {createSecureServer} = require('ob-core-common-modules');
const circuit = require('ob-core-circuit-breaker').circuit;
const {SecurityLogger, securityLoggerInit} = require('ob-core-security-logger');
const logger = require('ob-core-node-logger').app;
const {initCircuitBreakerAudit} = require('ob-core-audit-logger');

const {loadDataSource} = require('./utils/server-utils');
const {loggerInitialize} = require('./middleware/logger-middleware');
const {serverSetup, ddsLocation} = require('./config.json');

const app = loopback();



app._findLayerByHandler = function (handler) {
  // Other handlers can be added to the stack, for example,
  // NewRelic adds sentinel handler. We need to search the stack
  for (var k = this._router.stack.length - 1; k >= 0; k--) {
    if (this._router.stack[k].handle === handler ||
      // NewRelic replaces the handle and keeps it as __NR_original
      this._router.stack[k].handle['__NR_original'] === handler ||
      (this._router.stack[k].handle['__appdynamicsProxyInfo__'] && this._router.stack[k].handle['__appdynamicsProxyInfo__'].orig === handler)
    ) {
      return this._router.stack[k];
    } else {
      // Aggressively check if the original handler has been wrapped
      // into a new function with a property pointing to the original handler
      for (var p in this._router.stack[k].handle) {
        if (this._router.stack[k].handle[p] === handler) {
          return this._router.stack[k];
        }
      }
    }
  }
  return null;

};

/**
 * Security Logger Initialization
 */
SecurityLogger.initialize({
  transports: [{
    File: {
      filename: process.env.SECURITY_LOG_PATH || 'logs/security.log'
    }
  }]
});
securityLoggerInit(__filename);

/**
 * Datasource Directory Setup
 */
loadDataSource(ddsLocation, app);

loggerInitialize(app, logger);

/**
 * Server API Start
 */
app.start = (httpOnly) => {
  circuit.initialize(app, app.get('circuitBreaker').configPath, logger, app.get('circuitBreaker').enable);
  initCircuitBreakerAudit(app.get('auditCBConfigs'), circuit);

  let port = app.get('port');
  let host = app.get('host');
  let server = createSecureServer(app, {
    httpOnly: httpOnly,
    fileName: serverSetup.serverIdentity,
    joinFileName: serverSetup.joinFileName,
    secureProtocol: serverSetup.secureProtocol,
    requestCert: serverSetup.requestCert,
    rejectUnauthorized: serverSetup.rejectUnauthorized
  });


  server.listen(port, host, () => {
    app.emit('started');
    logger.info(`Web server listening explorer at: ${host} ${port}`);
  });
  return server;
};

module.exports = app;
