'use strict';
const {infoURL} = require('ob-core-common-modules');
const restApiRoot = require('../utils/getRootContext');

module.exports = (server) => {
// Install a `/` route that returns server status
    const router = server.loopback.Router();
    router.get('/', server.loopback.status());
    router.get(`${restApiRoot()}/info`,infoURL);
    server.use(router);
};
