'use strict';

let host = process.env.AUDIT_ENTERPRISE_SERVICE_V2_URL;
let operations = require('./operations.json');

operations = JSON.parse(new Function('host', `return \`${JSON.stringify(operations)} \`;`)(host));
module.exports = {
  name: 'audit',
  connector: 'rest',
  debug: 'true',
  operations
};
