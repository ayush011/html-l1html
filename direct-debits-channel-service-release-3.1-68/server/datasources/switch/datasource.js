'use strict';

// Get the host url from environment variable
let host = process.env.SWITCH_ENTERPRISE_SERVICE_URL || '';


let operations = require('./operations.json');

/*eslint-disable*/

operations = JSON.parse(new Function('host', `return \`${JSON.stringify(operations)} \`;`)(host));

/*eslint-enable*/
module.exports = {
  name: 'switch',
  connector: 'rest',
  debug: 'true',
  operations
};
