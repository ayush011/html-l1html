'use strict';

let host = process.env.DD_ENTERPRISE_SERVICE_URL;

let operations = require('./operations.json');

operations = JSON.parse(new Function('host', `return \`${JSON.stringify(operations)} \`;`)(host));

module.exports = {
  name: 'directDebits',
  connector: 'rest',
  operations
};
