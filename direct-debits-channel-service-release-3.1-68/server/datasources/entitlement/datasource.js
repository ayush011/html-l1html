'use strict';

let host = process.env.ETN_ENTERPRISE_SERVICE_URL;

let operations = require('./operations.json');

operations = JSON.parse(new Function('host', `return \`${JSON.stringify(operations)} \`;`)(host));

module.exports = {
  name: 'entitlement',
  connector: 'rest',
  operations
};
