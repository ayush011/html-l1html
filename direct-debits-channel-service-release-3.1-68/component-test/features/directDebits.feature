@sanity @regression @directDebitChannel @PSD2-5053 @PSD2-11570 @PSD2-12711

Feature: As a Direct Debit Channel Service Provider, I should be able to provide the Direct Debit records to the TPP successfully

  @PSD2-5053 @directDebitChannel-1 @sprint-9
  Scenario Outline: Verify that Direct Debit Channel API returns 403 "Forbidden error" incase given entitlement has not
  "ReadDirectDebit" permission or "invalid scope" or "invalid agreement response status on entitlement field"

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:
      | Header                                 | QueryParameter               | ErrorCode |
      | DirectDebit_Header_NoReadPermissions   | DirectDebit_2_QueryParameter | 403       |
      | DirectDebit_Header_Entitlement_Expired | DirectDebit_2_QueryParameter | 403       |
      | DirectDebit_Header_Entitlement_Suspend | DirectDebit_2_QueryParameter | 403       |
      | DirectDebit_Header_Entitlement_Created | DirectDebit_2_QueryParameter | 403       |
      | DirectDebit_Header_Invalid_Scope       | DirectDebit_2_QueryParameter | 403       |
      | DirectDebit_Header_AgreeRes_Invalid    | DirectDebit_1_QueryParameter | 403       |


  @PSD2-5053 @directDebitChannel-2 @sprint-9
  Scenario Outline: Verify that Direct Debit Channel API returns 500 "Unexpected Error" in case Entitlement ID and Account Ref ID mismatch

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then The directDebit channel API response error should match the swagger structure and with <Expected Response>

    Examples:
      | Header                            | QueryParameter               | Expected Response    |
      | DirectDebit_Entitlement_Not_Found | DirectDebit_1_QueryParameter | Error_500_Unexpected |


  @PSD2-5053 @directDebitChannel-3 @sprint-9
  Scenario Outline: Verify that Direct Debit Channel API returns 401 "Unauthorized" error w.r.t to the blank or invalid request header

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:
      | Header                               | QueryParameter               | ErrorCode |
      | DirectDebit_Blank_Entitlement_Header | DirectDebit_1_QueryParameter | 401       |


  @PSD2-5053 @directDebitChannel-4 @sprint-9
  Scenario Outline: Verify that Direct Debit Channel API returns 401 "Unauthorized error " incase of missing request header

    When I miss the header from the Customer Direct Debit API <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:
      | Header                     | QueryParameter                     | ErrorCode |
      | DirectDebit_Header_Default | DirectDebit_QueryParameter_Default | 401       |


  @PSD2-5053 @directDebitChannel-5 @sprint-9
  Scenario Outline: Verify that Direct Debit Channel API returns 500 "Unexpected Error" against CNF- 429 error code

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then The directDebit channel API response error should match the swagger structure and with <Expected Response>

    Examples:
      | Header                          | QueryParameter               | Expected Response    |
      | DirectDebit_Header_CNF_429_OBIE | DirectDebit_2_QueryParameter | Error_500_Unexpected |


  @PSD2-5053 @directDebitChannel-6 @sprint-9
  Scenario Outline: Verify that Direct Debit Channel API returns 500 "Unexpected Error" against CNF- 4xx error codes

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then The directDebit channel API response error should match the swagger structure and with <Expected Response>

    Examples:
      | Header                           | QueryParameter               | Expected Response    |
      | DirectDebit_Invalid_Header       | DirectDebit_1_QueryParameter | Error_500_Unexpected |
      | DirectDebit_Header_CNF_405_OBIE  | DirectDebit_2_QueryParameter | Error_500_Unexpected |
      | DirectDebit_Header_CNF_445_OBIE  | DirectDebit_2_QueryParameter | Error_500_Unexpected |
      | DirectDebit_Header_CNF_485_OBIE  | DirectDebit_2_QueryParameter | Error_500_Unexpected |
      | DirectDebit_Header_CNF_495_OBIE  | DirectDebit_2_QueryParameter | Error_500_Unexpected |
      | DirectDebit_Header_Invalid_Brand | DirectDebit_2_QueryParameter | Error_500_Unexpected |
      | DirectDebit_Header_Blank_Brand   | DirectDebit_2_QueryParameter | Error_500_Unexpected |
      | DirectDebit_Header_Default       | DirectDebit_6_QueryParameter | Error_500_Unexpected |


  @PSD2-11570 @directDebitChannel-7 @sprint-10
  Scenario Outline: Verify that Get Account Direct Debit Channel API retrieves relevant Direct Debit records from the "E050"

    Given I fetch the Direct Debit Channel API details from the expected response file <Expected Response>
    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get all information as per the Direct Debit channel schema
    Then I should get the Customers Direct Debit records correctly

    Examples:

      | Expected Response            | QueryParameter               | Header                                 |
      | directDebit_ManyRecords      | DirectDebit_1_QueryParameter | DirectDebit_Header_Lloyds_ManyRecords  |
      | directDebit_ManyRecords      | DirectDebit_1_QueryParameter | DirectDebit_Header_BoS_ManyRecords     |
      | directDebit_ManyRecords      | DirectDebit_1_QueryParameter | DirectDebit_Header_Halifax_ManyRecords |
      | directDebit_ZeroRecord       | DirectDebit_1_QueryParameter | DirectDebit_Header_Lloyds_ZeroRecord   |
      | directDebit_ZeroRecord       | DirectDebit_1_QueryParameter | DirectDebit_Header_BoS_ZeroRecord      |
      | directDebit_ZeroRecord       | DirectDebit_1_QueryParameter | DirectDebit_Header_Halifax_ZeroRecord  |
      | directDebit_ZeroRecord       | DirectDebit_1_QueryParameter | DirectDebit_Header_ZeroRecord          |
      | directDebit_NoPrevPay_Record | DirectDebit_8_QueryParameter | DirectDebit_Header_Missing_PrevPayment |


  @PSD2-11570 @directDebitChannel-8 @sprint-10
  Scenario Outline: Verify that Direct Channel API returns 401 "Unauthorized" error w.r.t to the space in request header

    When I fetch the Customer Direct Debit details by passing without space in <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:
      | Header                               | QueryParameter               | ErrorCode |
      | DirectDebit_Space_Entitlement_Header | DirectDebit_1_QueryParameter | 401       |


  @PSD2-6239 @directDebitChannel-9 @sprint-10
  Scenario Outline: CBS Error codes should be displayed with respect to the mock data set at "E050" service

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:

      | Header                            | QueryParameter               | ErrorCode |
      | DirectDebit_Header_CBSErrorCode   | DirectDebit_1_QueryParameter | 500       |
      | DirectDebit_Header_CBSErrorCode_1 | DirectDebit_2_QueryParameter | 401       |
      | DirectDebit_Header_CBSErrorCode_2 | DirectDebit_3_QueryParameter | 403       |
      | DirectDebit_Header_CBSErrorCode_3 | DirectDebit_4_QueryParameter | 500       |
      | DirectDebit_Header_CBSErrorCode_4 | DirectDebit_5_QueryParameter | 500       |


  @PSD2-5053 @directDebitChannel-10 @sprint-9 @defect
  Scenario Outline: Verify that Direct Debit Channel API returns 500 "Internal Server Error" against CNF- 5xx error codes

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:
      | Header                          | QueryParameter               | ErrorCode |
      | DirectDebit_Header_CNF_505_OBIE | DirectDebit_2_QueryParameter | 500       |
      | DirectDebit_Header_CNF_515_OBIE | DirectDebit_2_QueryParameter | 500       |
      | DirectDebit_Header_CNF_565_OBIE | DirectDebit_2_QueryParameter | 500       |


  @PSD2-4678 @PSD2-4679 @directDebitChannel-11 @sprint-12
  Scenario Outline: Verify that direct debit channel API contains 'Content Type' in Response Header

    Given I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then The direct debit channel API should return 'content-Type' in header response <ResponseHeader>

    Examples:
      | QueryParameter               | Header                                | ResponseHeader                |
      | DirectDebit_1_QueryParameter | DirectDebit_Header_Lloyds_ManyRecords | TE_ResponseHeader_ContentType |


  @PSD2-19831 @directDebitChannel-12 @sprint-0
  Scenario Outline: Verify that Get Account Direct Debit Channel API returns the correct response for those direct debits where no values are returned for AUDDISRefVl & OrigRefTx field from the "E050"

    Given I fetch the Direct Debit Channel API details from the expected response file <Expected Response>
    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get all information as per the Direct Debit channel schema
    Then I should get the Customers Direct Debit records correctly

    Examples:

      | Header                                                    | QueryParameter               | Expected Response                           |
      | DirectDebit_Header_Lloyds_SomeWithNoMandateIdentification | DirectDebit_7_QueryParameter | directDebit_SomeWithNoMandateIdentification |

  @PSD2-19831 @directDebitChannel-13 @sprint-0
  Scenario Outline: Verify that Get Account Direct Debit Channel API returns the correct response for those direct debits those direct debits where no values are returned for AUDDISRefVl & OrigRefTx field from the "E050"

    Given I fetch the Direct Debit Channel API details from the expected response file <Expected Response>
    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get all information as per the Direct Debit channel schema
    Then I should get the Customers Direct Debit records correctly

    Examples:

      | Header                                                   | QueryParameter               | Expected Response                          |
      | DirectDebit_Header_Lloyds_AllWithNoMandateIdentification | DirectDebit_9_QueryParameter | directDebit_AllWithNoMandateIdentification |


  @directDebitChannel-14 @sprint-17
  Scenario Outline: Verify that directDebit Channel API returns 400 "Invalid account" in case given entitlement has
  "inactive agreement resource status on entitlement field"

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then The directDebit channel API response error should match the swagger structure and with <Expected Response>

    Examples:
      | Header                                                   | QueryParameter                | Expected Response            |
      | DirectDebit_Header_Lloyds_AllWithNoMandateIdentification | DirectDebit_Invalid_AccountId | Error_400_Invalid_Account_Id |


  @directDebitChannel-15 @sprint-17
  Scenario Outline: Verify that directDebit Channel API returns 500 "Unexpected Error" in case given given header param has invalid enum value
  for required value

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then The directDebit channel API response error should match the swagger structure and with <Expected Response>

    Examples:
      | Header                           | QueryParameter               | Expected Response    |
      | DirectDebit_Header_Invalid_Brand | DirectDebit_7_QueryParameter | Error_500_Unexpected |

  @directDebitChannel-16
  Scenario Outline: Verify that Direct Debit Channel API returns 401 "Unauthorized" error when entitlement version is 4.0.0 and service version is 3.0.0

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:
      | Header                               | QueryParameter               | ErrorCode |
      | DirectDebit_Unsupported_Version_Header | DirectDebit_1_QueryParameter | 401       |
  @directDebitChannel-17
  Scenario Outline: Verify that Direct Debit Channel API returns 401 "Unauthorized" error when entitlement version is 4.0.0 and service version is 3.0.0

    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an error with status code <ErrorCode> for Direct Debit Channel

    Examples:
      | Header                               | QueryParameter               | ErrorCode |
      | DirectDebit_Unsupported_Version_Header2 | DirectDebit_1_QueryParameter | 401       |

  Scenario Outline: Verify that Get Account Direct Debit Channel API return empty data set when channel is commercial from the "E050"

    Given I fetch the Direct Debit Channel API details from the expected response file <Expected Response>
    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an empty data set

    Examples:

      | Expected Response              | QueryParameter               | Header                                            |
      | directDebit_CommercialResponse | DirectDebit_1_QueryParameter | DirectDebit_Header_Lloyds_Commercial_ManyRecords  |
      | directDebit_CommercialResponse | DirectDebit_1_QueryParameter | DirectDebit_Header_BOS_Commercial_ManyRecords     |

@directDebitChannel-19
  Scenario Outline: Verify that Get Account Direct Debit Channel API return empty response when product category is credit card, both PCC and BCC  

    Given I fetch the Direct Debit Channel API details from the expected response file <ExpectedResponse>
    When I fetch the Customer Direct Debit details by passing valid <Header> and <QueryParameter>
    Then I should get an empty data set

    Examples:

      | ExpectedResponse              | QueryParameter                  | Header                                            |
      | directDebitResponseForCC      | DirectDebit_1_QueryParameter    | DirectDebit_Header_Lloyds_CreditCard2             | 
      | directDebitResponseForCC      | DirectDebit_1_QueryParameterBCC | DirectDebit_Header_Lloyds_CreditCard              |