try {
  const assertDiff = require('assert-diff');
  const helper = require('../../server/utils/helper');
  const chai = require('chai');
  const expect = chai.expect;

  const commonServiceLib = require('ob-core-bdd').commonServiceLib;
  const apiConfig = require('ob-core-bdd').apiConfig;
  const jsonValidationLib = require('ob-core-bdd').jsonValidationLib;

  const apiEndPoint = process.env.API_ENDPOINT;
  const endpointREST = '/accounts/%AccountReferenceID%/direct-debits';

  // to build local server endpoint
  let endPoint;
  if (process.env.LOCAL_BDD === 'true') {
    const app = require('../../server/server');
    const apiRoot = app.get('restApiRoot');
    const host = app.get('host');
    const port = app.get('port');
    endPoint = `http://${host}:${port}${apiRoot}${endpointREST}`;
  } else if (apiEndPoint !== 'undefined') {
    const apiRootContextPath = '/cs-direct-debits/v3.1';
    endPoint = `https://${apiEndPoint}.lbg.eu-gb.mybluemix.net${apiRootContextPath}${endpointREST}`;
  } else {
    throw ('please specify API_ENDPOINT as a environment with appropriate API endpoint or gulp run-local, gulp run-local-wm');
  }

  console.log('Endpoint -->> ', endPoint);

  const dataFolder = apiConfig.config.expectedResponseFolderPath;
  const schemaFolder = `${apiConfig.config.schemaYAMLFolderPath}ob-aisp-direct-debits-channel-service.yaml`;
  const header = require(apiConfig.config.request).header;
  const query = require(apiConfig.config.request).query;

  const defaultQueryParameterKey = 'DirectDebit_QueryParameter_Default';
  const completeQueryParameterData = query;
  let expJsondata;
  let headerData;
  let responseAPI;
  let schemavalidationResults;

  const defaultHeaderKey = 'DirectDebit_Header_Default';
  const completeHeaderData = header;

  assertDiff.options.strict = true;

  module.exports = function () {
    this.Given(/^I fetch the Direct Debit Channel API details from the expected response file (.*)$/, responseFileName => {
      expJsondata = jsonValidationLib.readFileData(dataFolder, responseFileName);
      expJsondata = JSON.parse(JSON.stringify(expJsondata));
    });

    this.Given(/^I fetch the Customer Direct Debit details by passing valid (.*) and (.*)$/, function (header, queryParameter) {
      let endpoint = endPoint;
      endpoint = commonServiceLib.FormEndpoint(endpoint, completeQueryParameterData, defaultQueryParameterKey, queryParameter);
      console.log('url is....', endpoint);
      //Read the Header File
      headerData = commonServiceLib.GetHeaders(completeHeaderData, defaultHeaderKey, header);
      //Add all the Headers
      this.apiLib.addMultipleHeaders(headerData);
      //Send the Get request
      responseAPI = this.apiLib.sendRequest('GET', endpoint);
    });

    this.Given(/^I fetch the Customer Direct Debit details by passing without space in (.*) and (.*)$/, function (header, queryParameter) {
      let endpoint = endPoint;
      endpoint = commonServiceLib.FormEndpoint(endpoint, completeQueryParameterData, defaultQueryParameterKey, queryParameter);
      console.log('url is....', endpoint);
      //Read and trimming the Header File
      headerData = commonServiceLib.GetTrimHeaders(completeHeaderData, defaultHeaderKey, header);
      //Add all the Headers
      this.apiLib.addMultipleHeaders(headerData);
      //Send the Get request
      responseAPI = this.apiLib.sendRequest('GET', endpoint);
      console.log('@@@@@Response', responseAPI.body);
    });


    this.Given(/^I should get the Customers Direct Debit records correctly$/, () => {
      const result = responseAPI.body;
      assertDiff.deepEqual(expJsondata['Data'], JSON.parse(result)['Data']);
    });

    this.Given(/^I should get an empty data set$/, () => {
      const result = responseAPI.body;
      expect(JSON.parse(result)['Links']['Self']).to.not.empty;
      assertDiff.deepEqual(expJsondata['Data'], JSON.parse(result)['Data']);
    });

    this.Given(/^I should get an error with status code (.*) for Direct Debit Channel$/, errorCode => {
      expect(responseAPI.statusCode.toString()).to.equal(errorCode);
    });

    this.Given(/^I put the current date in the Direct Debit Channel file$/, () => {
      
      //Below code is changing the DateTime key value to the passed value in Direct DebitExpected response file
      expJsondata = JSON.parse(JSON.stringify(expJsondata));
    });

    this.Given(/^The directDebit channel API response error should match the swagger structure and with (.*)$/, responseFile => {
      expJsondata = jsonValidationLib.readFileData(dataFolder, responseFile);
      responseAPI.body = JSON.parse(responseAPI.body);
      assertDiff.deepEqual(expJsondata, responseAPI.body);
    });

    this.Given(/^I should get all information as per the Direct Debit channel schema$/, function () {
      const directDebitSchema = schemaFolder;
      schemavalidationResults = this.apiLib.validateResponseWithSchema(directDebitSchema);
      console.log(`Schema Results:${schemavalidationResults}`);
      expect(schemavalidationResults).to.equal(null);
    });

    this.Given(/^I miss the header from the Customer Direct Debit API (.*) and (.*)$/, function (header, queryParameter) {
      let endpoint = endPoint;
      //Read the Header File
      headerData = commonServiceLib.GetHeaders(completeHeaderData, defaultHeaderKey, header);
      this.apiLib.skipHeaders(headerData, 'x-lbg-entitlement-token');
      endpoint = commonServiceLib.FormEndpoint(endpoint, completeQueryParameterData, defaultQueryParameterKey, queryParameter);
      //Send the Get request
      responseAPI = this.apiLib.sendRequest('GET', endpoint);
    });

    this.Given(/^The direct debit channel API should return 'content-Type' in header response (.*)$/, resHeader => {
      const resHeaderData = completeHeaderData[resHeader];
      //assertDiff.deepEqual(responseAPI.headers['content-type'], resHeaderData['content-type']);
      expect(responseAPI.headers['content-type']).to.equal(resHeaderData['content-type']);
      console.log('@@@@@@@Response Header::', resHeaderData['content-type']);
    });
  };
} catch (err) {
  console.log('Error found in beneficiary.js File');
  console.log(err);
}
