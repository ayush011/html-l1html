module.exports = {
    development: {
     'account-services': 'http://shared-jenk-12.sandbox.local:9073/v1/entitlements',
    'direct-debit-services': 'http://shared-jenk-12.sandbox.local:9073/DirectDebits'

  },
  test: {
    'account-services': 'http://shared-jenk-12.sandbox.local:9073/v1/entitlements',
    'direct-debit-services': 'http://shared-jenk-12.sandbox.local:9073/DirectDebits'

  }
};
