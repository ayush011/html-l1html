const Jasmine = require('jasmine');
const fs = require('fs-extra');
const JasmineConsoleReporter = require('jasmine-console-reporter');
dataSourceNinjaIn();
const circuit = require('ob-core-circuit-breaker').circuit;
const {initCircuitBreakerAudit} = require('ob-core-audit-logger');
const logger = require('ob-core-node-logger').app;
const app = require('../server/server');

circuit.initialize(app, app.get('circuitBreaker').configPath, logger, app.get('circuitBreaker').enable);
initCircuitBreakerAudit(app.get('auditCBConfigs'), circuit);

const reporter = new JasmineConsoleReporter({
  colors: 2,           // (0|false)|(1|true)|2
  cleanStack: 1,       // (0|false)|(1|true)|2|3
  verbosity: 4,        // (0|false)|1|2|(3|true)|4
  listStyle: 'indent', // "flat"|"indent"
  activity: false
});


const jasmine = new Jasmine();
jasmine.loadConfigFile('./specs/jasmine.json');
jasmine.addReporter(reporter);
jasmine.execute();

jasmine.onComplete(() => dataSourceNinjaOut());


//add test specific model config to server folder before running test suite
function dataSourceNinjaIn() {
  fs.copySync('specs/server.json', 'server.json');
}

//remove test specific model config from server folder after running test suite
function dataSourceNinjaOut() {
  fs.removeSync('server.json');
}
