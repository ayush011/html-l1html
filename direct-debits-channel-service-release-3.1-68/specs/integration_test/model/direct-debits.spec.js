'use strict';

const assert = require('chai').assert;
const expect = require('chai').expect;
const supertest = require('supertest');
const moment = require('moment');
const app = require('../../../server/server');
const expected = require('../expected/read-direct-debits.json');
const commercialExpected = require('../expected/commercial-direct-debits.json');
const assertDiff = require('assert-diff');
const directDebitsData = require('../mocks/DirectDebits.json');
const mockEntitlementData = require('../mocks/Entitlement.json');
const emptyResponseForCC=require('../expected/empty-response-for-cc');

const DirectDebit = app.models.DirectDebits;
const EaadsDirectDebit = app.models.EaadsDirectDebits;
const Entitlement = app.models.Entitlement;

const CTX_PATH = '/cs-direct-debits/v3.1';

DirectDebit.getDirectDebitsDetails = function (accountNumber, sortCode, correlationID, brand, fovIndicator, cb) {
  let result = directDebitsData.filter(directDebit => {
    if (directDebit.AccountNumber === accountNumber && directDebit.SortCode === sortCode) {
      return directDebit;
    }
  });
  if (!result.length) {
    cb(null, directDebitsData[0]);
  }
  cb(null, result[0]);
};

EaadsDirectDebit.getDirectDebitsDetails = function (accountNumber, sortCode, correlationID, brand, fovIndicator, cb) {
  let result = directDebitsData.filter(directDebit => {
    if (directDebit.AccountNumber === accountNumber && directDebit.SortCode === sortCode) {
      return directDebit;
    }
  });
  if (!result.length) {
    cb(null, directDebitsData[0]);
  }
  cb(null, result[0]);
};

describe('DirectDebits models', () => {
  beforeEach(function () {

    Entitlement.getEntitlement = function (authorizationCode, correlationID, userRole, fovIndicator, brand, channel, fapiID, cb) {

      let result = mockEntitlementData.filter(r => {
        if (r.EntitlementAccessCode === authorizationCode) {
          return r;
        }
      });

      cb(null, result[0]);
    };
  });

  beforeAll(() => {
    // switching off consent expiry check before running test cases 
    process.env.UNRESTRICTED_ACCESS_DURATION= "0";
  });

  it('should have a getDirectDebits function defined', (done) => {
    expect(DirectDebit.getDirectDebits).to.be.a('function');
    done();
  });

  it('should have a getAccountTransactions function defined', (done) => {
    expect(DirectDebit.getDirectDebitsDetails).to.be.a('function');
    done();
  });

  it('should call the getDirectDebits and return expected output', (done) => {
    const authorization = 'test-entitlement-token';
    const accountId = '3e9b951e-318f-11e7-93ae-92361f002671';
    const correlationID = 'abcd-1234-efgh-5678';
    const brand = 'LYDS';
    const userRole = 'CUSTOMER';
    const channel = 'RC';
    const fapiID = 'fapiID';
    const hostName = 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds';
    const req = {};
    const fovIndicator = true;
    const cb = (err, data) => {
      if (err) {
        done.fail();
      } else {
        assertDiff.deepEqual(data, expected, 'actual data is equal to expected data');
        done();
      }
    };
    DirectDebit.getDirectDebits(authorization, accountId, correlationID, brand, channel, userRole, fapiID, hostName, fovIndicator, req, cb);
  });


});

describe('Confirm entitlement status and scope for account DirectDebits service provider', () => {
  beforeEach(function () {

    Entitlement.getEntitlement = function (authorizationCode, correlationID, userRole, fovIndicator, brand, channel, fapiID, cb) {

      let result = mockEntitlementData.filter(r => {
        if (r.EntitlementAccessCode === authorizationCode) {
          // Entitlement created date time is changed to the current time so that the consent does not expire and the test cases pass. 1 hour is added for DST case so as to handle PSD2-68830
          // r.CreatedDateTime= moment().isDST()? moment().add(1, 'hour') : moment();
          return r;
        }
      });

      cb(null, result[0]);
    };
  });

  beforeAll(() => {
    // switching off consent expiry check before running test cases 
    process.env.UNRESTRICTED_ACCESS_DURATION= "0";
  });

  it('should return a 200 message corresponding to Authorisation ID', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.equal(res.statusCode, 200, 'done');
          done();
        }
      });
  });

  it('should return a 200 status but empty direct debits', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f00/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token2')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.equal(res.statusCode, 200, 'done');
          done();
        }
      });
  });

  it('should return a 401 status if the Authorization header is missing in request headers', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.equal(res.statusCode, 401, 'done');
          done();
        }
      });
  });

  it('should return a 401 status when the Authorization header have whitespaces only in request headers', (done) => {
    const expectedError = {error: {statusCode: 401, message: 'Unauthorised'}};
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f00/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', '                  ')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.deepEqual(res.body, expectedError);
          assert.equal(res.statusCode, 401, 'done');
          done();
        }
      });
  });

  it('should return a 403 status if the Entitlement Status as Retrieved Status', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002675/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token1')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.equal(res.statusCode, 403, 'done');
          done();
        }
      });
  });

  it('should return a 403 status if the Entitlement as we do not have proper permissions', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002675/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token1')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.equal(res.statusCode, 403, 'done');
          done();
        }
      });
  });

  it('should return a 401 status if the Authorization header is empty in request headers', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', '')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.equal(res.statusCode, 401, 'done');
          done();
        }
      });
  });


  it('should return a 500 status if brand is not valid', (done) => {
    const expectedError = {
      Code: '500 UnexpectedError',
      Message: 'Unexpected error',
      Errors: [{
        ErrorCode: 'UK.OBIE.UnexpectedError',
        Message: 'Due to a technical problem, the request cannot be completed.'
      }]
    };
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'INVALID')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.deepEqual(res.body, expectedError);
          assert.equal(res.statusCode, 500, 'done');
          done();
        }
      });
  });

  it('should return a 500 error when length of param is more than the max length', (done) => {
    const expectedError = {
      Code: '500 UnexpectedError',
      Message: 'Unexpected error',
      Errors: [{
        ErrorCode: 'UK.OBIE.UnexpectedError',
        Message: 'Due to a technical problem, the request cannot be completed.'
      }]
    };

    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', '3e9b951e-318f-11e7-93ae-92361f0026713e9b951e-318f-11e7-93ae-92361f0026713e9b951e-318f-11e7-93ae-92361f0026713e9b951e-318f-11e7-93ae-92361f0026713e9b951e-318f-11e7-93ae-92361f0026713e9b951e-3test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.deepEqual(res.body, expectedError);
          assert.equal(res.statusCode, 500, 'done');
          done();
        }
      });
  });

  it('should return a 400 error for invalid accountID', (done) => {

    const expectedError = {
      Code: '400 BadRequest',
      Message: 'Invalid request parameters',
      Errors: [{
        ErrorCode: 'UK.OBIE.Field.Invalid',
        Message: 'AccountId not valid.'
      }]
    };

    supertest(app)
      .get(`${CTX_PATH}/accounts/invalid/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.deepEqual(res.body, expectedError);
          assert.equal(res.statusCode, 400, 'done');
          done();
        }
      });
  });

  it('should return a 500 error when userRole is not matching any enum', (done) => {

    const expectedError = {
      Code: '500 UnexpectedError',
      Message: 'Unexpected error',
      Errors: [{
        ErrorCode: 'UK.OBIE.UnexpectedError',
        Message: 'Due to a technical problem, the request cannot be completed.'
      }]
    };

    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'INVALID')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.deepEqual(res.body, expectedError);
          assert.equal(res.statusCode, 500, 'done');
          done();
        }
      });
  });


  it('should return a 200 status if x-fapi-customer-last-logged-time is given in correct format', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .set('x-fapi-customer-last-logged-time', 'Tue, 20 Mar 2018 10:16:17 UTC')
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assert.equal(res.statusCode, 200, 'done');
          done();
        }
      });
  });

  it('should return expected output as empty response for credit card product (PCC)', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002675/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'da350aaa-290a-4f69-a852-27ef3cbaa1ed')
      .set('x-lbg-txn-correlation-id', 'test')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'test')
      .set('x-lbg-origin-host', 'test')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assertDiff.deepEqual(res.body.Data, emptyResponseForCC.Data, 'actual data is equal to expected data');
          assert.notEqual(res.body.Links.Self, '', 'self link should not be empty');
          assert.equal(res.statusCode, 200, 'done');
          done();
        }
      });
  });

  it('should return expected output as empty response for credit card product (BCC)', (done) => {
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002611/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', '9c842777-b775-44a5-a633-5cbe031d7adi')
      .set('x-lbg-txn-correlation-id', 'test')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'test')
      .set('x-lbg-origin-host', 'test')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assertDiff.deepEqual(res.body.Data, emptyResponseForCC.Data, 'actual data is equal to expected data');
          assert.notEqual(res.body.Links.Self, '', 'self link should not be empty');
          assert.equal(res.statusCode, 200, 'done');
          done();
        }
      });
  });


  it('should return a 400 status if x-fapi-customer-last-logged-time is given in wrong format', (done) => {
    const expected = {
      Code: '400 BadRequest',
      Message: 'Invalid header parameters',
      Errors: [{
        ErrorCode: 'UK.OBIE.Header.Invalid',
        Message: 'x-fapi-customer-last-logged-time is invalid format. Please refer to the OBIE specs.'
      }]
    };

    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .set('x-fapi-customer-last-logged-time', 'Invalid')
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assertDiff.deepEqual(res.body, expected, 'actual data is equal to expected data');
          assert.equal(res.statusCode, 400, 'done');
          done();
        }
      });
  });


  it('should return a 400 error when required field (x-fapi-financial-id) is not passed', (done) => {
    const expectedError = {
      Code: '400 BadRequest',
      Message: 'Invalid request parameters',
      Errors: [{
        ErrorCode: 'UK.OBIE.Field.Missing',
        Message: 'x-fapi-financial-id is missing in the request.'
      }]
    };
    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assertDiff.deepEqual(res.body, expectedError, 'actual data is equal to expected data');
          assert.equal(res.statusCode, 400, 'done');
          done();
        }
      });
  });

  it('should return a 500 error when required field (x-lbg-txn-correlation-id) is not passed', (done) => {
    const expectedError = {
      Code: '500 UnexpectedError',
      Message: 'Unexpected error',
      Errors: [{
        ErrorCode: 'UK.OBIE.UnexpectedError',
        Message: 'Due to a technical problem, the request cannot be completed.'
      }]
    };

    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-origin-host', 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assertDiff.deepEqual(res.body, expectedError, 'actual data is equal to expected data');
          assert.equal(res.statusCode, 500, 'done');
          done();
        }
      });
  });

  it('should return a 500 error when required field (x-lbg-origin-host) is not passed', (done) => {
    const expectedError = {
      Code: '500 UnexpectedError',
      Message: 'Unexpected error',
      Errors: [{
        ErrorCode: 'UK.OBIE.UnexpectedError',
        Message: 'Due to a technical problem, the request cannot be completed.'
      }]
    };

    supertest(app)
      .get(`${CTX_PATH}/accounts/3e9b951e-318f-11e7-93ae-92361f002671/direct-debits`)
      .set('Accept', 'application/json')
      .set('x-lbg-entitlement-token', 'test-entitlement-token')
      .set('x-lbg-txn-correlation-id', 'abcd-1234-efgh-5678')
      .set('x-lbg-internal-user-role', 'CUSTOMER')
      .set('x-lbg-brand', 'LYDS')
      .set('x-lbg-channel', 'RC')
      .set('x-fapi-financial-id', 'FAPI')
      .set('x-lbg-fov-indicator', true)
      .end((err, res) => {
        if (err) {
          done.fail();
        } else {
          assertDiff.deepEqual(res.body, expectedError, 'actual data is equal to expected data');
          assert.equal(res.statusCode, 500, 'done');
          done();
        }
      });
  });
});

describe('Confirm Commercial channel request return correct response', () => {
  beforeEach(function () {

    Entitlement.getEntitlement = function (authorizationCode, correlationID, userRole, fovIndicator, brand, channel, fapiID, cb) {

      let result = mockEntitlementData.filter(r => {
        if (r.EntitlementAccessCode === authorizationCode) {
          // Entitlement created date time is changed to the current time so that the consent does not expire and the test cases pass. 1 hour is added for DST case so as to handle PSD2-68830
          // r.CreatedDateTime= moment().isDST()? moment().add(1, 'hour') : moment();
          return r;
        }
      });

      cb(null, result[0]);
    };
  });

  beforeAll(() => {
    // switching off consent expiry check before running test cases 
    process.env.UNRESTRICTED_ACCESS_DURATION= "0";
  });

  it('should call the getDirectDebits and return expected output for Commercial channel', (done) => {
    const authorization = 'test-entitlement-token';
    const accountId = '3e9b951e-318f-11e7-93ae-92361f002671';
    const correlationID = 'abcd-1234-efgh-5678';
    const brand = 'LYDS';
    const userRole = 'CUSTOMER';
    const channel = 'CC';
    const fapiID = 'fapiID';
    const hostName = 'https://gatewaydata01.psd2.sandbox.extranet.group/cma-phase-2-orange/lbg/open-banking/lyds';
    const req = {};
    const fovIndicator = true;
    const cb = (err, data) => {
      if (err) {
        done.fail();
      } else {
        assertDiff.deepEqual(data, commercialExpected, 'actual data is equal to expected data');
        done();
      }
    };
    DirectDebit.getDirectDebits(authorization, accountId, correlationID, brand, channel, userRole, fapiID, hostName, fovIndicator, req, cb);
  });
});
