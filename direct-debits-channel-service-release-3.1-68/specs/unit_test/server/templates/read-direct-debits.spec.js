'use strict';

const assertDiff = require('assert-diff');

const template = require('../../../../server/templates/read-direct-debits');
const mock = require('../../../integration_test/mocks/DirectDebits.json');

describe('Direct Debits Template', () => {
  it('should return MandateIdentification as NOT AVAILABLE when both AUDDISRefValue and RefText are missing in enterprise response ', done => {
    const accountId = '3e9b951e-318f-11e7-93ae-92361f002671';
    try {
      let output = template(mock[2].DirectDebits[0], accountId);
      assertDiff.deepEqual(output.MandateIdentification, 'NOT AVAILABLE');
      done();
    } catch (e) {
      done.fail();
    }
  });

  it('should return Name as NOT AVAILABLE when Name is missing in enterprise response ', done => {
    const accountId = '3e9b951e-318f-11e7-93ae-92361f002671';
    try {
      let output = template(mock[2].DirectDebits[0], accountId);
      assertDiff.deepEqual(output.Name, 'NOT AVAILABLE');
      done();
    } catch (e) {
      done.fail();
    }
  });
});
