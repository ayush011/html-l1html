const expect = require('chai').expect;
const supertest = require('supertest');
const app = require('../../../server/server');
const api = supertest('http://localhost'+':'+ app.get('port'));
describe('server.js', () => {
        it('should return app function', (done) => {
            expect(app).to.be.an('Function');
            done();
        });
        it('Check for server status', function(done) {
            api.get('/')
                .set('Accept', 'application/json')
                .expect('Content-Type', '/json/')
                .expect(200, done);
        });
});

