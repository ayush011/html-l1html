'use strict';

const assert = require('chai').assert;
const filter = require('../../../../server/filters').directDebitsFilter;
const assertDiff = require('assert-diff');
const mock = require('../../../integration_test/mocks/Entitlement.json');
const remoteCtx = {
  set: function (key, value) {
    this[key] = value
  }
};


describe('Direct Debits filters', () => {

  it('should return empty array as entitlement object we got is null', (done) => {
    const accountId = "3e9b951e-318f-11e7-93ae-92361f002671";
    const data = filter(accountId, null, remoteCtx);

    assertDiff.deepEqual(data, [], "this should fail");
    done();
  });

  it('should return array as correct entitlement passed', (done) => {
    const accountId = "3e9b951e-318f-11e7-93ae-92361f002671";
    const data = filter(accountId, mock[0].EntitledAgreementResources, remoteCtx);

    assert.strictEqual(data.length, 1);
    done();
  });
});
