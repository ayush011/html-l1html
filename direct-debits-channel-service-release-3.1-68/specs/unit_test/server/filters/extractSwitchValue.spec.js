const assert = require('chai').assert;
const filter = require('../../../../server/filters/extractSwitchValue');
const mockSwitches = require('../mock-data/switch-api/switches.json');
const switchNameEnum = require('../../../../server/enums/switch-constants').SWITCH_NAME;

describe('ExtractSwitchValue Filter', () => {
    it('should return the switch value from mock data we have provided', () => {
        const output = filter(mockSwitches, switchNameEnum);
        assert.equal(mockSwitches[0]['SwitchOn'], output, 'switch value mismatch');
    });
});
