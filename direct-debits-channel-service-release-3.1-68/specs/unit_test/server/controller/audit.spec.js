'use strict';

const audit = require('../../../../server/controller/audit');
const ctx = { active: { http: { req: { headers: { } } } } };

const mockAudit = {
    circuitBreaker: {
        execute: function(model, name, params){
            return Promise.resolve(true);
        }
    }
}
const auditRecord = {
    requestBody: { },
    requestHeaders: { }
};

describe('Audit', () => {
  it('should call audit service', (done) => {
    audit(mockAudit, auditRecord, ctx).then(function(){
        done();
    })
  });
});
