const auditHelper = require('../../../../server/utils/auditHelpers');

const logAuditEvent = auditHelper.logAuditEvent;
var Promise = require("bluebird");

describe('auditHelpers', () => {

  it('should be defined', () => {
    expect(logAuditEvent).toBeDefined();
  });

  it('should log audit event for commercial', (done) => {
    const entitlement = require('../mock-data/entitlement-api/validEntitlement.json');

    const ctx = {
      active: {
        http: {
          req: {
            headers: {
              'x-lbg-ob-tpp-org-id': 'abc',
              'x-lbg-client-id': 'cdf',
              'x-lbg-client-app-name': 'entitlement',
              'x-lbg-txn-correlation-id': '123456',
              'x-lbg-fov-indicator': 'cc',
              'x-lbg-channel': 'CC',
              'x-lbg-brand': 'LYDS'
            },
            query: {
              fromBookingDateTime: '2011-01-11',
              toBookingDateTime: '2011-01-12'
            }
          }
        }
      },
      get: function (key) {
        if (key === 'audit_log_data') {
          return this[key];
        } else {
          return entitlement;
        }
      },
      set: function (key, value) {
        this[key] = value;
        return this;
      }
    };

    const promise = new Promise(function(resolve){
      resolve('CCEventLogged');
      done();
    });

    const Audit = {
      circuitBreaker: {
        execute: function(){
          return promise;
        }
      }
    };

    logAuditEvent(ctx, null, Audit);
  });
});
