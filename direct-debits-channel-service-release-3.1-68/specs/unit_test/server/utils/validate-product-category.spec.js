const expect = require('chai').expect;

const {checkProductCategoryValidity} = require('../../../../server/utils/validate-product-category');
const entitlementForPCC = require('../../../integration_test/mocks/EntitlementForPCC.json');
const entitlementForPCA = require('../../../integration_test/mocks/EntitlementForPCA.json');

describe('check Product Category Validity', () => {

    it('should have checkProductCategoryValidity as fucntion', (done) => {
        expect(checkProductCategoryValidity).to.be.an('Function');
        done();
    });

    it('should return false for product category PCC', (done) => {
        const result = checkProductCategoryValidity(entitlementForPCC, '3e9b951e-318f-11e7-93ae-92361f002675', null);
        expect(result).to.equal(false);
        done();
    });

    it('should return true for product category PCA', (done) => {
        const result = checkProductCategoryValidity(entitlementForPCA, '3e9b951e-318f-11e7-93ae-92361f002675', null);
        expect(result).to.equal(true);
        done();
    });

});
