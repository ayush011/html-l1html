'use strict';
var fs = require('fs');
const path = require('path');
const {updateCache} = require('../../../../server/utils/cacheController');
const switchesJson = require('../../../../server/enums/switches.json');
const mockSwitches = require('../mock-data/switch-api/switches.json');
const assertDiff = require('assert-diff');

describe('Cache controller', () => {

    it('should return the mock passed to updateCache util as output', (done)=>{
        const output = updateCache(mockSwitches);
        assertDiff.deepEqual(output, mockSwitches, 'there is a json mismatch');
        done();
    });

    it('should update the switchesJson file with the mock data provided', (done) => {
        let switchesPath = path.resolve(__dirname,'../../../../server/enums/switches.json');
        let updatedSwitchesJson = fs.readFileSync(switchesPath, {encoding:'utf8', flag:'r'});
        assertDiff.deepEqual(JSON.parse(updatedSwitchesJson)['switches'], mockSwitches, 'there is a json mismatch');
        done();
    });

    afterAll(()=>{
        let switchesPath = path.resolve(__dirname,'../../../../server/enums/switches.json');
        fs.writeFileSync(switchesPath, JSON.stringify(switchesJson));
    });
});
