const expect = require('chai').expect;
const logger = require('ob-core-node-logger').app;
const app = require('../../../../server/api');
const {loadDataSource} = require('../../../../server/utils/server-utils');
describe('server util', () => {

    it('should have loadAppdynamics as fucntion', (done) => {
        expect(loadDataSource).to.be.an('Function');
        done();
    });

    it('should not load datasource as nothing ddsLocation is not passed while loading datasource', (done) => {
        loadDataSource(null, app);
        done();
    });
});

