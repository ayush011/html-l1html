'use strict';

const {securityErrorHandler} = require('../../../../server/utils/security-logger');

describe('Security logger', () => {

  it('should security log if entitlment is not authorised log error', () => {
    const error = {status: 401};
    securityErrorHandler(error);
  });

  it('should security log if entitlment is not having valid scope and status log error', () => {
    const error = {status: 403};
    securityErrorHandler(error);
  });

  it('should security log if entitlment is not valid log error', () => {
    const error = {status: 404};
    securityErrorHandler(error);
  });

  it('should not security log for any other error', () => {
    const error = {statusCode: 400};
    securityErrorHandler(error);
  });
});
