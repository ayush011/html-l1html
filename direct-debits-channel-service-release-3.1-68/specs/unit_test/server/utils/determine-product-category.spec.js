const expect = require('chai').expect;

const {detemineProductCategory} = require('../../../../server/utils/determine-product-category');
const entitlementForPCC = require('../../../integration_test/mocks/FilteredDirectDebitsForPCC.json');
const entitlementForPCA = require('../../../integration_test/mocks/FilteredDirectDebitsForPCA.json');
const  emptyGroup = require('../../../../server/enums/direct-debits-constants').EMPTY_RESPONSE_GROUP;

describe('detemine Product Category', () => {

    it('should have detemineProductCategory as fucntion', (done) => {
        expect(detemineProductCategory).to.be.an('Function');
        done();
    });

    it('should return false for product category PCC', (done) => {
        const result = detemineProductCategory(emptyGroup, entitlementForPCC);
        expect(result).to.equal(false);
        done();
    });

    it('should return true for product category PCA', (done) => {
        const result = detemineProductCategory(emptyGroup, entitlementForPCA);
        expect(result).to.equal(true);
        done();
    });
});
